module bitbucket.org/pingcloud/go-db

go 1.15

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/shopspring/decimal v1.2.0
)
