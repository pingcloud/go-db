package damp

import (
	"reflect"
)

// Field this struct holds all the data needed for the insert
type Field struct {
	Name        string
	Kind        reflect.Kind
	Value       interface{}
	Placeholder string
}

// getFieldsFromIface the columns from the type that needs to be saved
func getFieldsFromIface(value reflect.Value, only []string) []*Field {
	fields := make([]*Field, 0)

	for i := 0; i < value.NumField(); i++ {
		// Check if db column name is defined, if not, skip it.
		columnName, ok := value.Type().Field(i).Tag.Lookup("db")
		if !ok || (columnName == "" || columnName == "-") {
			continue
		}

		// Check if field
		if only != nil && !contains(only, columnName) {
			continue
		}

		// Check if index tag has been set, if 0, skip it.
		if index, ok := value.Type().Field(i).Tag.Lookup("index"); ok {
			if (index == "pk" || index == "fk") && value.Field(i).Int() == 0 {
				continue
			}
		}

		vf := value.Field(i)
		if t, ok := vf.Interface().(Time); ok {
			if _, ok := value.Type().Field(i).Tag.Lookup("on_create"); ok && t.IsZero() {
				vf = reflect.ValueOf(Now())
			} else if _, ok := value.Type().Field(i).Tag.Lookup("on_update"); ok {
				vf = reflect.ValueOf(Now())
			} else if t.IsZero() {
				continue
			} else {
				vf = reflect.ValueOf(t.UTC())
			}
		}

		// Column definition
		field := new(Field)
		field.Name = columnName
		field.Kind = vf.Kind()
		field.Placeholder = ":" + columnName
		field.Value = vf.Interface()

		fields = append(fields, field)
	}

	return fields
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}

	return false
}
