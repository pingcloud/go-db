package damp

import (
	"database/sql"
	"fmt"

	// used by the sql connector
	_ "github.com/go-sql-driver/mysql"
)

var registry map[string]interface{}
var dbParams DBConnectionParams

type DBConnectionParams struct {
	User 		string
	Password 	string
	Host 		string
	Port 		int
	Name 		string
	Charset 	string
	Driver		string
}

// Open a new connection to the database
func db() (*sql.DB, error) {
	if dbParams == (DBConnectionParams{}) {
		return nil, fmt.Errorf("DB connection params not set")
	}

	connectionString := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=%s",
		dbParams.User,
		dbParams.Password,
		dbParams.Host,
		dbParams.Port,
		dbParams.Name,
		dbParams.Charset,
	)

	db, err := sql.Open(dbParams.Driver, connectionString)
	if err != nil {
		return nil, err
	}

	return db, nil
}

// Sets the connection details for the database
func SetDBConnectionParams(params DBConnectionParams) {
	dbParams = params
}

// SetRegistry fills the registry with connections to the users structs
func SetRegistry(r map[string]interface{}) {
	registry = r
}

// getRegistryEntry returns an item from the registry
func getRegistryEntry(name string) interface{} {
	entry, ok := registry[name]
	if !ok {
		panic("registry entry not found")
	}

	return entry
}

// doSelectView rows from the database without binding it to a struct
func doSelectView(query string) ([]interface{}, error) {
	rows, err := selectRows(query)
	if err != nil {
		return nil, err
	}

	return dataMap(rows)
}

// doSelect rows from the database
func doSelect(query string, i interface{}) ([]interface{}, error) {
	rows, err := selectRows(query)
	if err != nil {
		return nil, err
	}

	return dataBind(rows, i)
}

// doInsert data into the db
func doInsert(i interface{}) (int64, error) {
	// Create insert query from interface
	query := createInsertQuery(i)

	// open the database
	db, err := db()
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}
	defer db.Close()

	// Execute the query
	res, err := db.Exec(query)
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}

	// Get the last inserted ID
	id, err := res.LastInsertId()
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}

	// Return the ID
	return id, nil
}

// doInsertAll ...
func doInsertAll(i ...interface{}) []int64 {
	ids := make([]int64, 0)

	for _, obj := range i {
		if id, err := doInsert(obj); err == nil {
			ids = append(ids, id)
		}
	}

	return ids
}

// doUpdate data into the db
func doUpdate(query string) (int64, error) {
	// open the database
	db, err := db()
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}
	defer db.Close()

	// Execute the query
	res, err := db.Exec(query)
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}

	return res.RowsAffected()
}

// doDelete data in the database
func doDelete(query string) (int64, error) {
	// open the database
	db, err := db()
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}
	defer db.Close()

	// Execute the query
	res, err := db.Exec(query)
	if err != nil {
		return sql.NullInt64{}.Int64, err
	}

	return res.RowsAffected()
}

func selectRows(query string) (*sql.Rows, error) {
	// open the database
	db, err := db()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	// Execute the query
	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}

	return rows, nil
}

func dataMap(rows *sql.Rows) ([]interface{}, error) {
	// Close rows when done
	defer rows.Close()

	// Create an empty slice of interfaces
	objects := make([]interface{}, 0)

	for rows.Next() {
		object, err := Extract(rows)
		if err != nil {
			return objects, err
		}

		objects = append(objects, object)
	}

	return objects, nil
}

func dataBind(rows *sql.Rows, i interface{}) ([]interface{}, error) {
	// Close rows when done
	defer rows.Close()

	// Create an empty slice of interfaces
	objects := make([]interface{}, 0)

	// Loop over the results from the database
	for rows.Next() {
		object, err := Bind(i, rows)
		if err != nil {
			return objects, err
		}

		objects = append(objects, object)
	}

	return objects, nil
}
