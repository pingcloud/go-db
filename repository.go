package damp

import (
	"fmt"
	"reflect"
	"strings"
)

type scalar struct {
	N int64 `db:"n"`
}

// Find ...
func Find(i interface{}, id int64) (interface{}, error) {
	pk, _ := getPrimaryKey(reflect.ValueOf(i))
	if pk == "" {
		return nil, fmt.Errorf("no primary key found")
	}

	return FindOneBy(i, Criteria(
		Equals(pk, id),
	))
}

// ConvertAndCorrelate ...
func ConvertAndCorrelate(iface interface{}) (interface{}, error) {
	var rv = reflect.ValueOf(iface)
	if rv.Kind() != reflect.Ptr {
		rv = Pointer(iface)
	}
	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	jsonData := make(map[string]interface{})

	for i := 0; i < rv.NumField(); i++ {
		t := rv.Type().Field(i)
		f := rv.Field(i)
		d, hasDbName := t.Tag.Lookup("db")
		if !hasDbName || d == "-" {
			continue
		}
		// if d == "correlation_id" {
		// 	continue
		// }
		_, hasIndex := t.Tag.Lookup("index")
		if hasIndex && f.Int() == 0 {
			continue
		}
		if gt, ok := f.Interface().(Time); ok && gt.IsZero() {
			continue
		}

		if m, ok := t.Tag.Lookup("cor"); ok {
			object, err := Find(getRegistryEntry(m), rv.Field(i).Int())
			if err != nil {
				return nil, err
			}
			p := reflect.ValueOf(object)
			if p.Interface() == nil {
				return nil, fmt.Errorf("trying to get correlation ID for `%s`, but failed because the object is nil", m)
			}

			correlationID := p.FieldByName("CorrelationID").Interface()
			z := false // Is the correlationID zero?
			switch correlationID.(type) {
			case int:
				z = correlationID == int(0)
			case int32:
				z = correlationID == int32(0)
			case int64:
				z = correlationID == int64(0)
			}

			if z {
				return nil, fmt.Errorf("CorrelationID of relationship `%s` is 0", m)
			}

			jsonData[d] = correlationID
		} else {
			jsonData[d] = f.Interface()
		}
	}

	return jsonData, nil
}

// Save handles a save action for a model
func Save(i interface{}) error {
	id, err := doInsert(i)
	if err != nil {
		return err
	}

	rv := reflect.ValueOf(i)
	if rv.Type().Kind() == reflect.Ptr {
		rv = rv.Elem()
	}
	for j := 0; j < rv.NumField(); j++ {
		field := rv.Field(j)
		fieldType := rv.Type().Field(j)
		if index, ok := fieldType.Tag.Lookup("index"); ok && index == "pk" {
			field.SetInt(id)
		}
	}

	emit(i, "created")

	return nil
}

// Update handles an update action for a model
func Update(i interface{}) error {
	pk, id := getPrimaryKey(reflect.ValueOf(i))
	if pk == "" || id == nil {
		return fmt.Errorf("no primary key found")
	}

	_, err := UpdateBy(i, Criteria(
		Equals(pk, id),
	))
	if err != nil {
		return err
	}

	return nil
}

// UpdateFields ...
func UpdateFields(i interface{}, fields []*Field) error {
	pk, id := getPrimaryKey(reflect.ValueOf(i))
	if pk == "" || id == nil {
		return fmt.Errorf("no primary key found")
	}

	qb := NewQuery(UPDATESTMT)
	qb.setTableName(getTableNameFromIface(i))
	qb.fields = fields
	q := buildCriteria(qb, Criteria(
		Equals(pk, id),
	))

	_, err := doUpdate(q)
	if err != nil {
		return err
	}

	return nil
}

// FindAll the rows
func FindAll(i interface{}) ([]interface{}, error) {
	qb := buildBaseQuery(i)
	q := qb.getSQL()

	return doSelect(q, i)
}

// FindBy all that match the criteria
func FindBy(i interface{}, criteria []Clause) ([]interface{}, error) {
	qb := buildBaseQuery(i)
	q := buildCriteria(qb, criteria)

	return doSelect(q, i)
}

// FindByOrdered all that match the criteria and order it
func FindByOrdered(i interface{}, criteria []Clause, ordering map[string]string) ([]interface{}, error) {
	qb := buildBaseQuery(i)
	buildOrdering(qb, ordering)
	q := buildCriteria(qb, criteria)

	return doSelect(q, i)
}

// FindOneBy that matches the criteria
func FindOneBy(i interface{}, criteria []Clause) (interface{}, error) {
	qb := buildBaseQuery(i)
	qb.AddLimit(1)
	q := buildCriteria(qb, criteria)

	return getSingleResult(doSelect(q, i))
}

// FindOneByOrdered that matches the criteria and order it
func FindOneByOrdered(i interface{}, criteria []Clause, ordering map[string]string) (interface{}, error) {
	qb := buildBaseQuery(i)
	qb.AddLimit(1)
	buildOrdering(qb, ordering)
	q := buildCriteria(qb, criteria)

	return getSingleResult(doSelect(q, i))
}

// Count rows that matches the criteria
func Count(i interface{}, criteria []Clause) (int64, error) {
	qb := buildBaseQuery(i)
	qb.Select("count(id) as n")
	q := buildCriteria(qb, criteria)

	r, err := getSingleResult(doSelect(q, scalar{}))
	if err != nil {
		return 0, err
	}

	v := reflect.ValueOf(r)

	return v.FieldByName("N").Int(), nil
}

// First ...
func First(i interface{}, criteria []Clause) (interface{}, error) {
	qb := buildBaseQuery(i)
	qb.AddOffset(0)
	qb.AddLimit(1)

	q := buildCriteria(qb, criteria)

	return getSingleResult(doSelect(q, i))
}

// UpdateBy a struct
func UpdateBy(i interface{}, criteria []Clause) (int64, error) {
	qb := NewQuery(UPDATESTMT)
	qb.setTableName(getTableNameFromIface(i))
	qb.setFields(i)
	q := buildCriteria(qb, criteria)

	rowsAffected, err := doUpdate(q)
	if err != nil {
		return 0, err
	}

	emit(i, "updated")

	return rowsAffected, nil
}

// DeleteBy a struct
func DeleteBy(i interface{}, criteria []Clause) (int64, error) {
	qb := NewQuery(DELETESTMT)
	qb.setTableName(getTableNameFromIface(i))
	qb.setFields(i)
	q := buildCriteria(qb, criteria)

	rowsAffected, err := doUpdate(q)
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}

// Get results from database with a custom built SQL string
func Get(sql string) ([]interface{}, error) {
	return doSelectView(sql)
}

// GetAndBind results from database and bind it to the struct
func GetAndBind(sql string, i interface{}) ([]interface{}, error) {
	return doSelect(sql, i)
}

// View ...
func View(from string, criteria []Clause, selectors ...string) ([]interface{}, error) {
	qb := NewQuery(SELECTSTMT)
	qb.setTableName(from)

	// if no selectors
	if len(selectors) == 0 {
		selectors = append(selectors, "*")
	}

	qb.Select(selectors...)

	var q string
	if criteria != nil {
		q = buildCriteria(qb, criteria)
	} else {
		q = qb.getSQL()
	}

	return doSelectView(q)
}

// Relations ...
func Relations(parent reflect.Value) interface{} {
	if parent.Kind() == reflect.Ptr {
		parent = parent.Elem()
	}

	for i := 0; i < parent.NumField(); i++ {
		t := parent.Type().Field(i)
		if r, ok := t.Tag.Lookup("related"); ok {
			if parent.Field(i).Kind() == reflect.Ptr {
				OneToOne(parent, getRegistryEntry(r), t.Name)
			} else if parent.Field(i).Kind() == reflect.Slice {
				OneToMany(parent, getRegistryEntry(r), t.Name)
			}
		}
	}

	return parent.Interface()
}

// OneToOne ...
func OneToOne(rv reflect.Value, c interface{}, join string) error {
	pk, id := getPrimaryKey(rv)
	if pk == "" || id == nil {
		return fmt.Errorf("no primary key found")
	}

	object, err := FindOneBy(c, Criteria(
		Equals(pk, id),
	))
	if err != nil {
		return err
	}
	if object == nil {
		return nil
	}

	po := Pointer(object)
	Relations(po)

	rv.FieldByName(join).Set(po)

	return err
}

// OneToMany ...
func OneToMany(rv reflect.Value, c interface{}, join string) error {
	pk, id := getPrimaryKey(rv)
	if pk == "" || id == nil {
		return fmt.Errorf("no primary key found")
	}

	objects, err := FindBy(c, Criteria(
		Equals(pk, id),
	))
	if err != nil {
		return err
	}

	for j := 0; j < len(objects); j++ {
		po := Pointer(objects[j])
		Relations(po)

		rv.FieldByName(join).Set(reflect.Append(rv.FieldByName(join), po))
	}

	return nil
}

// Pointer ...
func Pointer(i interface{}) reflect.Value {
	nv := reflect.New(reflect.TypeOf(i)).Interface()
	nrv := reflect.ValueOf(nv).Elem()
	rv := reflect.ValueOf(i)

	for j := 0; j < rv.NumField(); j++ {
		nrv.Field(j).Set(rv.Field(j))
	}

	return reflect.ValueOf(nv)
}

func getPrimaryKey(rv reflect.Value) (string, interface{}) {
	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	for i := 0; i < rv.NumField(); i++ {
		ft := rv.Type().Field(i)
		f := rv.Field(i)

		if t, ok := ft.Tag.Lookup("index"); ok && t == "pk" {
			return ft.Tag.Get("db"), f.Interface()
		}
	}

	return "", nil
}

func getSingleResult(results []interface{}, err error) (interface{}, error) {
	if err != nil {
		return nil, err
	}
	if len(results) == 0 {
		return nil, nil
	}

	return results[0], nil
}

func getTableNameFromIface(i interface{}) string {
	v := reflect.ValueOf(i)
	if v.Kind() == reflect.Ptr {
		i = v.Elem().Interface()
	}

	t := reflect.TypeOf(i)
	n := Underscore(t.Name())

	return strings.ToLower(n)
}
