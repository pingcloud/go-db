package damp

import (
	"encoding/json"

	"github.com/shopspring/decimal"
)

// Decimal wrapper for db.Decimal struct
type Decimal struct {
	decimal.Decimal
}

// NewDecimal returns a new decimal from an int64
func NewDecimal(i int64) Decimal {
	return Decimal{
		Decimal: decimal.New(i, 0),
	}
}

// NewDecimalFromFloat return a new decimal from an float64
func NewDecimalFromFloat(f float64) Decimal {
	return Decimal{
		Decimal: decimal.NewFromFloat(f),
	}
}

// ZeroDecimal returns a zero decimal
func ZeroDecimal() Decimal {
	return Decimal{
		Decimal: decimal.Zero,
	}
}

// Add overrides the addition function of decimal.Decimal
func (d Decimal) Add(d2 Decimal) Decimal {
	return Decimal{
		Decimal: d.Decimal.Add(d2.Decimal),
	}
}

// Sub overrides the subtraction function of decimal.Decimal
func (d Decimal) Sub(d2 Decimal) Decimal {
	return Decimal{
		Decimal: d.Decimal.Sub(d2.Decimal),
	}
}

// Mul overrides the multiplication function of decimal.Decimal
func (d Decimal) Mul(d2 Decimal) Decimal {
	return Decimal{
		Decimal: d.Decimal.Mul(d2.Decimal),
	}
}

// Div overrides the division function of decimal.Decimal
func (d Decimal) Div(d2 Decimal) Decimal {
	return Decimal{
		Decimal: d.Decimal.Div(d2.Decimal),
	}
}

// Round overrides the round function of decimal.Decimal
func (d Decimal) Round(p int32) Decimal {
	return Decimal{
		Decimal: d.Decimal.Round(p),
	}
}

// Equals overrides the equals function of decimal.Decimal
func (d Decimal) Equals(d2 Decimal) bool {
	return d.Decimal.Equals(d2.Decimal)
}

// MarshalJSON override for default marshalling of decimal.Decimal
func (d Decimal) MarshalJSON() ([]byte, error) {
	f, _ := d.Float64()
	return json.Marshal(f)
}
