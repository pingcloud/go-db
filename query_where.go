package damp

const (
	// And ...
	And string = "AND"
	// Or ...
	Or string = "OR"
)

// On ...
type On struct {
	condition string
	glue      string
}

// AndWhere adds an AND where clause to the query
func (q *Query) AndWhere(condition string) *Query {
	return q.where(condition, And)
}

// OrWhere adds an OR where clause to the query
func (q *Query) OrWhere(condition string) *Query {
	return q.where(condition, Or)
}

// Add the where clause to the query
func (q *Query) where(condition, glue string) *Query {
	o := On{condition: condition, glue: glue}
	q.clauses = append(q.clauses, o)

	return q
}
