package damp

import (
	"fmt"
	"strconv"
	"time"
)

// GetTime converts functions for time
func GetTime(i interface{}) (time.Time, bool) {
	if t, ok := i.(time.Time); ok {
		return t, t.IsZero() == false
	}

	return time.Time{}, false
}

// FormatTime ...
func FormatTime(t time.Time) string {
	return t.Format("2006-01-02 15:04:05")
}

// QuoteValue quotes the string value to sanitize any malicious data
func QuoteValue(value string) string {
	return strconv.Quote(value)
}

// quoteName quotes the string value
func quoteName(value string) string {
	return fmt.Sprintf("`%s`", value)
}

// StringToTime byte conversions
func StringToTime(s string) (time.Time, error) {
	t, err := time.Parse("2006-01-02 15:04:05", s)
	if err == nil {
		loc, _ := time.LoadLocation("Europe/Amsterdam")
		return t.In(loc), nil
	}

	return time.Now(), err
}
