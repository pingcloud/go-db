package damp

// setTableName sets the table name
func (q *Query) setTableName(tableName string) *Query {
	q.tableName = tableName

	return q
}

// TableNameWithAlias sets the table name with an alias
func (q *Query) TableNameWithAlias(tableName, alias string) *Query {
	q.tableName = tableName
	q.alias = alias

	return q
}
