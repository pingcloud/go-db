package damp

type orderBy struct {
	column, direction string
}

// AddAscOrderBy ...
func (q *Query) AddAscOrderBy(column string) *Query {
	q.AddOrderBy(column, "ASC")

	return q
}

// AddDescOrderBy ...
func (q *Query) AddDescOrderBy(column string) *Query {
	q.AddOrderBy(column, "DESC")

	return q
}

// AddOrderBy ...
func (q *Query) AddOrderBy(column, direction string) *Query {
	q.orderBy = append(q.orderBy, orderBy{column: column, direction: direction})

	return q
}
