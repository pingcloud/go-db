package damp

import "fmt"

const (
	_equals      string = "%s = :%s"        // equals
	_notEquals   string = "%s <> :%s"       // not equals
	_isNull      string = "%s IS NULL"      // is null
	_isNotNull   string = "%s IS NOT NULL"  // is not null
	_greaterThan string = "%s > :%s"        // greater than
	_lowerThan   string = "%s < :%s"        // lower than
	_notIn       string = "%s NOT IN (:%s)" // not in
	_in          string = "%s IN (:%s)"     // in
)

// Clause holds conditional data for the where statement of a query
type Clause struct {
	Template string
	Field    string
	Value    interface{}
}

func buildCriteria(qb *Query, criteria []Clause) string {
	for _, c := range criteria {
		switch c.Template {
		case _equals, _notEquals, _greaterThan, _lowerThan, _in, _notIn:
			qb.AndWhere(fmt.Sprintf(c.Template, quoteName(c.Field), c.Field))
		case _isNull, _isNotNull:
			qb.AndWhere(fmt.Sprintf(c.Template, quoteName(c.Field)))
		}
	}

	params := NewParams(qb)
	for _, c := range criteria {
		if c.Value == nil {
			continue
		}
		params.Add(":"+c.Field, c.Value)
	}

	return params.GetQuery()
}

// Criteria adds clauses to the criteria set
func Criteria(criteria ...Clause) []Clause {
	return criteria
}

// Equals adds a condition: x = y
func Equals(field string, value interface{}) Clause {
	return Clause{Template: _equals, Field: field, Value: value}
}

// NotEquals adds a condition: x <> y
func NotEquals(field string, value interface{}) Clause {
	return Clause{Template: _notEquals, Field: field, Value: value}
}

// IsNull adds a condition: x is null
func IsNull(field string) Clause {
	return Clause{Template: _isNull, Field: field}
}

// In adds a condition: x in (values)
func In(field string, values ...interface{}) Clause {
	return Clause{Template: _in, Field: field, Value: values}
}

// NotIn adds a condition: x not in (values)
func NotIn(field string, values ...interface{}) Clause {
	return Clause{Template: _notIn, Field: field, Value: values}
}

// LowerThan adds a condition: x < value
func LowerThan(field string, value interface{}) Clause {
	return Clause{Template: _lowerThan, Field: field, Value: value}
}
