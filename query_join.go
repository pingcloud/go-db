package damp

const (
	// LeftJoin ...
	LeftJoin = "LEFT JOIN"
	// InnerJoin ...
	InnerJoin = "INNER JOIN"
)

type join struct {
	joinType  string
	from      string
	alias     string
	on        []On
	aggregate bool
}

// LeftJoin adds a left join statement to the query
func (q *Query) LeftJoin(from, alias string, on []On) *Query {
	j := join{joinType: LeftJoin, from: from, alias: alias, on: on}
	q.joins = append(q.joins, j)

	return q
}

// LeftJoinAggregate ...
func (q *Query) LeftJoinAggregate(from, alias string, on []On) *Query {
	j := join{joinType: LeftJoin, from: from, alias: alias, on: on, aggregate: true}
	q.joins = append(q.joins, j)

	return q
}

// InnerJoin adds a inner join statement to the query
func (q *Query) InnerJoin(from, alias string, conditions []On) *Query {
	j := join{joinType: InnerJoin, from: from, alias: alias, on: conditions}
	q.joins = append(q.joins, j)

	return q
}

// JoinOn conditions
func JoinOn(conditions ...string) []On {
	if len(conditions) == 0 {
		panic("You'll need to provide at least one condition.")
	}

	cons := make([]On, len(conditions))
	for i, con := range conditions {
		cons[i] = On{condition: con, glue: And}
	}

	return cons
}
