package damp

import (
	"database/sql"
	"reflect"
	"strconv"
	"time"

	"github.com/shopspring/decimal"
)

// Bind ...
func Bind(i interface{}, rows *sql.Rows) (interface{}, error) {
	fieldMap, err := createFieldMap(rows)
	if err != nil {
		return nil, err
	}

	return dataBinding(i, fieldMap), nil
}

// Extract ...
func Extract(rows *sql.Rows) (map[string]interface{}, error) {
	fieldMap, err := createFieldMap(rows)
	if err != nil {
		return nil, err
	}

	return castValue(fieldMap)
}

func createFieldMap(rows *sql.Rows) (map[string]interface{}, error) {
	// Get all the available columns
	columnNames, _ := rows.Columns()

	// Create columns with pointers for the column's value
	var columns = make([]interface{}, len(columnNames))
	var pointers = make([]interface{}, len(columnNames))
	for i := range columns {
		pointers[i] = &columns[i]
	}

	// Scan each row of the result and set pointers to the column's value
	if err := rows.Scan(pointers...); err != nil {
		return nil, err
	}

	// Set a referenced copy of the value in the fieldMap
	fieldMap := make(map[string]interface{})
	for i, columnName := range columnNames {
		val := pointers[i].(*interface{})
		fieldMap[columnName] = *val
	}

	return fieldMap, nil
}

func dataBinding(i interface{}, fieldMap map[string]interface{}) interface{} {
	valueElement := reflect.New(reflect.ValueOf(i).Type()).Elem()

	for i := 0; i < valueElement.NumField(); i++ {
		field := valueElement.Field(i)
		fieldType := valueElement.Type().Field(i)
		fieldDbName := fieldType.Tag.Get("db")

		if !field.CanSet() || fieldMap[fieldDbName] == nil {
			continue
		}

		value := string(fieldMap[fieldDbName].([]uint8))
		parsedValue := parseValue(field.Interface(), value)

		rv := reflect.ValueOf(parsedValue)
		for rv.Kind() == reflect.Ptr || rv.Kind() == reflect.Interface {
			rv = rv.Elem()
		}

		if field.IsValid() {
			field.Set(rv)
		}
	}

	return valueElement.Interface()
}

//
func castValue(fieldMap map[string]interface{}) (map[string]interface{}, error) {
	for name, value := range fieldMap {
		if value == nil {
			fieldMap[name] = value
			continue
		}

		v := string(value.([]uint8))

		// check if it is an int value
		if n, err := strconv.Atoi(v); err == nil {
			fieldMap[name] = n
		} else if b, err := strconv.ParseBool(v); err == nil {
			fieldMap[name] = b
		} else if f, err := strconv.ParseFloat(v, 64); err == nil {
			fieldMap[name] = f
		} else if t, err := StringToTime(v); err == nil {
			fieldMap[name] = t
		} else {
			fieldMap[name] = v
		}
	}

	return fieldMap, nil
}

// Parsing string value to its original type
func parseValue(i interface{}, value string) interface{} {
	switch i.(type) {
	case bool:
		v, _ := strconv.ParseBool(value)
		return v
	case int, int8, int16, int32, int64:
		v, _ := strconv.ParseInt(value, 10, 64)
		return v
	case float32, float64:
		v, _ := strconv.ParseFloat(value, 64)
		return v
	case string:
		return value
	case time.Time:
		v, _ := StringToTime(value)
		return v
	case Time:
		v, _ := CreateTime(value)
		return v
	case Decimal:
		d, _ := decimal.NewFromString(value)
		return Decimal{
			Decimal: d,
		}
	}

	return nil
}
