package damp

import "strings"

// Select adds select expression to the query
func (q *Query) Select(columns ...string) *Query {
	q.expression = strings.Join(columns, ", ")

	return q
}
