package damp

import (
	"reflect"
)

// observe - checks if the interface implements the method and calls it
func emit(i interface{}, event string) {
	var ptr reflect.Value
	var value reflect.Value
	var finalMethod reflect.Value
	var methodName string = "DbEvent"

	// Create value for point or pointer for value
	value = reflect.ValueOf(i)
	if value.Type().Kind() == reflect.Ptr {
		ptr = value
		value = ptr.Elem()
	} else {
		ptr = reflect.New(reflect.TypeOf(i))
		temp := ptr.Elem()
		temp.Set(value)
	}

	// Check if method exists and call it
	method := value.MethodByName(methodName)
	if method.IsValid() {
		finalMethod = method
	}
	// check for method on pointer
	method = ptr.MethodByName(methodName)
	if method.IsValid() {
		finalMethod = method
	}

	if finalMethod.IsValid() {
		finalMethod.Call([]reflect.Value{reflect.ValueOf(event)})
	}
}
