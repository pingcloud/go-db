package damp

import (
	"encoding/json"
	"time"
)

// Time wrapper for golang's Time
type Time struct {
	time.Time
}

// Now returns the current time
func Now() Time {
	return Time{
		Time: time.Now().UTC(),
	}
}

// CreateTime returns Time from string datetime
func CreateTime(value string) (Time, error) {
	t, err := time.Parse("2006-01-02 15:04:05", value)
	if err != nil {
		return Now(), err
	}
	return Time{
		Time: t,
	}, nil
}

// Localized returns localized time string in custom layout
func (t Time) Localized(layout string) string {
	// FIXME dynamic location
	loc, _ := time.LoadLocation("Europe/Amsterdam")

	return t.In(loc).Format(layout)
}

// Localized returns localized time string in custom layout
func (t Time) LocalizedZone(layout string, zone string) string {
	loc, _ := time.LoadLocation(zone)

	return t.In(loc).Format(layout)
}

// MarshalJSON is a custom implementation for MarshalJSON
func (t Time) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.Time.UTC().Format("2006-01-02 15:04:05"))
}

// UnmarshalJSON is a custom implementation for UnmarshalJSON
func (t *Time) UnmarshalJSON(b []byte) error {
	// First try the default
	def := &time.Time{}
	err := def.UnmarshalJSON(b)
	if err == nil {
		t.Time = *def
		return nil
	}

	// Test for null
	val := string(b)
	if val == "null" || val == "" {
		t.Time = Now().Time
		return nil
	}

	// Try normal
	parsed, err := time.Parse("2006-01-02 15:04:05", val)
	if err == nil {
		t.Time = parsed
		return nil
	}

	// Try extra ""
	parsed, err = time.Parse("\"2006-01-02 15:04:05\"", val)
	if err == nil {
		t.Time = parsed
		return nil
	}

	return err
}
