package damp

const caseSyntax = "CASE %s %s %s END"
const caseWhenThenSyntax = "WHEN %s THEN %s"

// CaseWrapper ...
type CaseWrapper struct {
	column     string
	conditions []CaseCondition
	elseValue  interface{}
	valueAs    string
}

// CaseCondition ...
type CaseCondition struct {
	when string
	then interface{}
}

// AddCase ...
func (q *Query) AddCase(column string, elseValue interface{}, valueAs string) *CaseWrapper {
	cf := new(CaseWrapper)
	cf.column = column
	cf.elseValue = elseValue
	cf.valueAs = valueAs

	q.caseWrappers = append(q.caseWrappers, cf)

	return cf
}

// NewCondition ...
func NewCondition(when string, then interface{}) CaseCondition {
	return CaseCondition{when: when, then: then}
}

// AddConditions ...
func (cf *CaseWrapper) AddConditions(conditions ...CaseCondition) *CaseWrapper {
	if len(conditions) == 0 {
		panic("You should at least give one caseCondition")
	}

	cf.conditions = append(cf.conditions, conditions...)

	return cf
}
