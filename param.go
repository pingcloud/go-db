package damp

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/shopspring/decimal"
)

// ParamFieldMap ...
type ParamFieldMap struct {
	stmt   string
	params []Field
}

// NewParams creates a new param field map
func NewParams(qb *Query) *ParamFieldMap {
	paramFieldMap := &ParamFieldMap{
		stmt: qb.getSQL(),
	}

	return paramFieldMap
}

// Add ...
func (paramFieldMap *ParamFieldMap) Add(placeholder string, i interface{}) *ParamFieldMap {
	param := Field{
		Placeholder: placeholder,
		Value:       i,
	}
	if i != nil {
		param.Kind = reflect.TypeOf(i).Kind()
	}

	paramFieldMap.params = append(paramFieldMap.params, param)

	return paramFieldMap
}

// GetQuery replaces all the parameters with real data
func (paramFieldMap *ParamFieldMap) GetQuery() string {
	for _, param := range paramFieldMap.params {
		paramToString := ParamToValue(param.Value)
		paramFieldMap.stmt = strings.Replace(paramFieldMap.stmt, param.Placeholder, paramToString, 1)
	}

	return paramFieldMap.stmt
}

// ParamToValue ...
func ParamToValue(i interface{}) string {
	if i == nil {
		return "null"
	}
	switch t := i.(type) {
	case bool:
		bitSetVat := int64(0)
		if i.(bool) {
			bitSetVat = 1
		}
		return strconv.FormatInt(bitSetVat, 10)
	case string:
		return QuoteValue(i.(string))
	case int:
		n := i.(int)
		n64 := int64(n)
		return strconv.FormatInt(n64, 10)
	case int64:
		return strconv.FormatInt(i.(int64), 10)
	case float64:
		return strconv.FormatFloat(i.(float64), 'f', 4, 64)
	case time.Time:
		if gt, ok := GetTime(i); ok {
			return QuoteValue(FormatTime(gt))
		}
	case Time:
		if gt, ok := i.(Time); ok {
			if !gt.IsZero() {
				return QuoteValue(FormatTime(gt.Time))
			}
		}
	case decimal.Decimal:
		return i.(decimal.Decimal).StringFixedBank(3)
	case Decimal:
		return i.(Decimal).StringFixedBank(3)
	case []interface{}:
		values := make([]string, 0)
		if s, ok := i.([]interface{}); ok {
			for _, v := range s {
				values = append(values, ParamToValue(v))
			}
		}
		return strings.Join(values, ",")
	default:
		panic(fmt.Sprintf("%T is not supported as parameter, yet.", t))
	}

	return ""
}
