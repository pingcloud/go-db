package damp

import (
	"fmt"
	"strings"
)

// buildInsertExpr join all the columns and values into one string
func buildInsertExpr(fields []*Field) string {
	names := make([]string, len(fields))
	values := make([]string, len(fields))

	for i, field := range fields {
		// Add column name to array
		names[i] = quoteName(field.Name)
		// Add value to array
		values[i] = ParamToValue(field.Value)
	}

	columnString := strings.Join(names, ", ")
	valuesString := strings.Join(values, ", ")

	return fmt.Sprintf("(%s) VALUES (%s)", columnString, valuesString)
}

// buildUpdateExpr builds the update string
func buildUpdateExpr(fields []*Field) string {
	updates := make([]string, len(fields))

	for i, field := range fields {
		// Add update field to array
		updates[i] = fmt.Sprintf("%s = %s", quoteName(field.Name), ParamToValue(field.Value))
	}

	return strings.Join(updates, ", ")
}

// buildGroupBy ...
func buildGroupBy(groupBy []string) string {
	return strings.Join(groupBy, ", ")
}

// buildWhereClauses builds all the where clauses
func buildWhereClauses(on []On) string {
	// if no where clauses, return empty string
	if len(on) == 0 {
		return ""
	}

	where := make([]string, 0)
	for key, clause := range on {
		if key != 0 {
			where = append(where, clause.glue)
		}
		where = append(where, clause.condition)
	}

	return strings.Join(where, " ")
}

// buildCaseExpr ...
func buildCaseExpr(controlFlows []*CaseWrapper) string {
	var caseSections = make([]string, 0)

	for _, flow := range controlFlows {
		var conditionSections = make([]string, 0)
		for _, con := range flow.conditions {
			conditionSections = append(
				conditionSections,
				fmt.Sprintf(caseWhenThenSyntax, con.when, ParamToValue(con.then)),
			)
		}

		var elseValue interface{}
		if flow.elseValue != nil {
			elseValue = "ELSE " + ParamToValue(flow.elseValue)
		} else {
			elseValue = ""
		}

		caseString := fmt.Sprintf(caseSyntax, flow.column, strings.Join(conditionSections, " "), elseValue)
		if len(flow.valueAs) != 0 {
			caseString = fmt.Sprintf("%s AS %s", caseString, flow.valueAs)
		}

		caseSections = append(caseSections, caseString)
	}

	return strings.Join(caseSections, ",")
}

// buildJoins builds the join statement
func buildJoins(joins []join) string {
	joinStatements := make([]string, len(joins))
	for i, join := range joins {
		// LEFT JOIN name AS alias ON on
		joinFrom := join.from
		if join.aggregate == false {
			joinFrom = quoteName(join.from)
		}
		joinStatements[i] = fmt.Sprintf(
			"%s %s AS %s ON %s",
			join.joinType,
			joinFrom,
			join.alias,
			buildWhereClauses(join.on),
		)
	}

	return strings.Join(joinStatements, " ")
}

// buildOrderBys ...
func buildOrderBys(sort []orderBy) string {
	if len(sort) == 0 {
		return ""
	}

	order := make([]string, 0)
	for _, clause := range sort {
		order = append(order, fmt.Sprintf("%s %s", clause.column, clause.direction))
	}

	return strings.Join(order, ", ")
}

// buildSelectExpr ...
func buildSelectExpr(q *Query) string {
	selectorSections := make([]string, 0)
	selectorSections = append(selectorSections, q.expression)
	if len(q.caseWrappers) != 0 {
		selectorSections = append(selectorSections, buildCaseExpr(q.caseWrappers))
	}

	return strings.Join(selectorSections, ", ")
}

// buildMethodExpr builds the from part of the query
func buildMethodExpr(tableName, alias, method string) string {
	methodSections := make([]string, 0)
	quotedTableName := quoteName(tableName)

	if method == SELECTSTMT {
		methodSections = append(methodSections, "FROM")
	}

	methodSections = append(methodSections, quotedTableName)

	if len(alias) > 0 {
		methodSections = append(methodSections, fmt.Sprintf("AS %s", alias))
	}

	return strings.Join(methodSections, " ")
}

// buildOrdering ...
func buildOrdering(qb *Query, ordering map[string]string) *Query {
	for col, dir := range ordering {
		qb.AddOrderBy(col, dir)
	}

	return qb
}

// buildBaseQuery ...
func buildBaseQuery(i interface{}) *Query {
	qb := NewQuery(SELECTSTMT)
	qb.Select("*")
	tableName := getTableNameFromIface(i)
	qb.setTableName(tableName)

	return qb
}
